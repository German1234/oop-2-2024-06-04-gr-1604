"""
return reversed text:
"asdf" -> "fdsa"
"asdf ghjk" -> "fdsa kjhg"
"asd25r" -> "rds25a"
"asd25r 1qwer2" -> "rds25a 1rewq2"
"""


def revers(text: str) -> str:
    # розбити на слова -> words: list[str]
    # перевернути кожне слово -> reversed_words: list[str]
    # зібрати слова в рядок -> reversed_text: str

    return text[::-1]


if __name__ == '__main__':
    cases = (
        ("asdf", "fdsa"),
        ("asdf ghjk", "fdsa kjhg"),
        ("asd25r", "rds25a"),
        ("asd25r 1qwer2", "rds25a 1rewq2"),
    )
    for text, expected in cases:
        assert revers(text) == expected, f"revers('{text}') = '{expected}', but got '{revers(text)}'"
